from django.urls import path
from account.api.views import UserCreateAPIView, UserList
from rest_framework.authtoken.views import obtain_auth_token

app_name = "account"

urlpatterns = [
    path('register/', UserCreateAPIView.as_view(), name="register"),
    path('login/', obtain_auth_token, name="register"),
    path('list/', UserList.as_view(), name="list_users")
]