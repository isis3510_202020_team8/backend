from account.models import Account
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view, permission_classes
from account.api.serializers import RegistrationSerializer, UserSerializer

class UserCreateAPIView(generics.CreateAPIView):
    queryset = Account.objects.all()
    serializer_class = RegistrationSerializer
    permission_classes = (AllowAny,)

class UserList(APIView):
    def get(self, request):
        users = Account.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)