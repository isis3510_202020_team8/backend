from django.db import models
from django.conf import settings

# Create your models here.


class Category(models.Model):
    NO_CATEGORY = 'NC'
    EDUCATIONAL = 'ED'
    HOBBIE = 'HB'
    NAME_CHOICES = [(EDUCATIONAL, "Educational"),
                    (HOBBIE, 'Hobbie'), (NO_CATEGORY, '')]
    POPULARITY_CHIOCES = [(i, i) for i in range(1, len(NAME_CHOICES))]
    DIFFICULTY_CHOICES = [(i, i) for i in range(11)]
    name = models.CharField(max_length=50, choices=NAME_CHOICES, unique=True)
    popularity = models.IntegerField(
        choices=POPULARITY_CHIOCES, default=len(NAME_CHOICES))
    difficulty = models.IntegerField(choices=DIFFICULTY_CHOICES)


class Goal(models.Model):
    name = models.CharField(max_length=50)
    desc = models.TextField()
    completed = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    due_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)


class Task(models.Model):
    name = models.CharField(max_length=50)
    desc = models.TextField()
    completed = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    due_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
