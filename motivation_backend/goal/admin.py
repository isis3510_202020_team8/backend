from django.contrib import admin
from goal.models import Goal, Task, Category
# Register your models here.

admin.site.register(Category)
admin.site.register(Task)
admin.site.register(Goal)
