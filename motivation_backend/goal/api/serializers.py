from rest_framework import serializers
from goal import models


class GoalSerializer(serializers.ModelSerializer):
    goal_owner = serializers.SerializerMethodField('get_goal_owner')
    goal_category = serializers.SerializerMethodField('get_goal_category')
    id = serializers.ReadOnlyField()
    date_created = serializers.ReadOnlyField()

    class Meta:
        model = models.Goal
        fields = ['id', 'name', 'desc', 'completed', 'date_created',
                  'due_date', 'goal_owner', 'goal_category']

    def get_goal_owner(self, goal):
        goal_owner = goal.user.username
        return goal_owner

    def get_goal_category(self, goal):
        goal_category = goal.category.name
        return goal_category


class TaskSerializer(serializers.ModelSerializer):
    task_goal = serializers.SerializerMethodField('get_task_goal')
    task_category = serializers.SerializerMethodField('get_task_category')
    date_created = serializers.ReadOnlyField

    class Meta:
        model = models.Task
        fields = ['name', 'desc', 'completed', 'date_created',
                  'due_date', 'task_goal', 'task_category']

    def get_task_goal(self, task):
        task_goal = task.goal.id
        return task_goal

    def get_task_category(self, task):
        task_category = task.category.name
        return task_category


class TaskInfoSerializer(serializers.ModelSerializer):
    task_goal = serializers.SerializerMethodField('get_task_goal')
    task_category = serializers.SerializerMethodField('get_task_category')
    task_owner = serializers.SerializerMethodField('get_task_owner')
    date_created = serializers.ReadOnlyField

    class Meta:
        model = models.Task
        fields = ['name', 'desc', 'completed', 'date_created',
                  'due_date', 'task_goal', 'task_category', 'task_owner']

    def get_task_owner(self, task):
        task_owner = task.goal.user.username
        return task_owner

    def get_task_goal(self, task):
        task_goal = task.goal.id
        return task_goal

    def get_task_category(self, task):
        task_category = task.category.name
        return task_category
