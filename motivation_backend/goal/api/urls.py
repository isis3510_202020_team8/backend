from django.urls import path
from goal.api.views import Goals, Tasks, GoalsDetail

app_name = "goal"

urlpatterns = [
    path('', Goals.as_view(), name="goals"),
    path('detail/<str:goal_id>/', GoalsDetail.as_view(), name="goals_detail"),
    path('tasks/', Tasks.as_view(), name="tasks"),
]
