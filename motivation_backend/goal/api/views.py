from rest_framework.response import Response
from rest_framework.views import APIView
from goal.api import serializers
from goal.models import Goal, Task, Category
from django.http import Http404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.parsers import FormParser, MultiPartParser
from account.models import Account


class Goals(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        goals = Goal.objects.filter(user=request.user)
        serializer = serializers.GoalSerializer(goals, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = serializers.GoalSerializer(data=request.data)
        if serializer.is_valid():
            category_name = request.data.get('goal_category')
            serializer.save(user=request.user,
                            category=Category.objects.get(name=category_name))
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GoalsDetail(APIView):
    def get(self, request, goal_id):
        try:
            goal = Goal.objects.get(pk=goal_id)
        except Goal.DoesNotExist:
            raise Http404
        serializer = serializers.GoalSerializer(goal)
        return Response(serializer.data)

    def put(self, request, goal_id):
        try:
            goal = Goal.objects.get(user=request.user, pk=goal_id)
        except Goal.DoesNotExist:
            raise Http404

        serializer = serializers.GoalSerializer(goal, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, goal_id):
        try:
            goal = Goal.objects.get(user=request.user, pk=goal_id)
        except Goal.DoesNotExist:
            raise Http404
        goal.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Tasks(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, goal_id):
        tasks = Task.objects.filter(pk=goal_id)
        serializer = serializers.TaskInfoSerializer(tasks, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = serializers.TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TasksDetail(APIView):
    def get(self, request, goal_id, task_id):
        try:
            task = Task.objects.get(goal_id=goal_id, pk=task_id)
        except Task.DoesNotExist:
            raise Http404
        serializer = serializers.TaskInfoSerializer(task)
        return Response(serializer.data)

    def put(self, request, goal_id, task_id):
        try:
            task = Task.objects.get(goal_id=goal_id, pk=task_id)
        except Task.DoesNotExist:
            raise Http404

        serializer = serializers.TaskSerializer(task, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, goal_id, task_id):
        try:
            task = Task.objects.get(goal_id=goal_id, pk=task_id)
        except Task.DoesNotExist:
            raise Http404
        task.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
